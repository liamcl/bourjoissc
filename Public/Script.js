//FiltersTrigger.js
// Event: Lens Initialized

// @input string Trigger = TapEvent { "widget": "combobox", "values": [ { "label": "Brows Lowered", "value": "BrowsLoweredEvent" }, { "label": "Brows Raised", "value": "BrowsRaisedEvent" }, { "label": "Brows Returned To Normal", "value": "BrowsReturnedToNormalEvent" }, { "label": "Face Found", "value": "FaceFoundEvent" }, { "label": "Face Lost", "value": "FaceLostEvent" }, { "label": "Kiss Finished", "value": "KissFinishedEvent" }, { "label": "Kiss Started", "value": "KissStartedEvent" }, { "label": "Mouth Closed", "value": "MouthClosedEvent" }, { "label": "Mouth Opened", "value": "MouthOpenedEvent" }, { "label": "Smile Finished", "value": "SmileFinishedEvent" }, { "label": "Smile Started", "value": "SmileStartedEvent" }, { "label": "Touch Start", "value": "TouchStartEvent" }, { "label": "Touch End", "value": "TouchEndEvent" }, { "label": "Tap", "value": "TapEvent" } ] }

// @ui {"widget":"separator"}

// @ui {"widget":"group_start", "label":"filter0"}

// @ui {"widget":"group_start", "label":"Animations"}
// @input Component.SpriteVisual f0_animation0_sq0 {"label": "Animation 0"}
// @ui {"widget":"group_end"}

// @input Component.SpriteVisual[] statics0 {"label": "Statics"}

// @ui {"widget":"group_end"}

// @ui {"widget":"separator"}

// @ui {"widget":"group_start", "label":"filter1"}

// @ui {"widget":"group_start", "label":"Animations"}
// @input Component.SpriteVisual f1_animation0_sq0 {"label": "Animation 0"}
// @input Component.SpriteVisual f1_animation1_sq0 {"label": "Animation 1"}
// @input Component.SpriteVisual f1_animation1_sq1 {"label": "Animation 2"}
// @ui {"widget":"group_end"}

// @input Component.SpriteVisual[] statics1 {"label": "Statics"}

// @ui {"widget":"group_end"}

// @ui {"widget":"separator"}

// @ui {"widget":"group_start", "label":"filter2"}

// @ui {"widget":"group_start", "label":"Animations"}
// @input Component.SpriteVisual f2_animation0_sq0 {"label": "Animation 0"}
// @input Component.SpriteVisual f2_animation1_sq0 {"label": "Animation 1"}
// @input Component.SpriteVisual f2_animation2_sq0 {"label": "Animation 2"}
// @ui {"widget":"group_end"}

// @input Component.SpriteVisual[] statics2 {"label": "Statics"}

// @ui {"widget":"group_end"}

// @ui {"widget":"separator"}

// @ui {"widget":"group_start", "label":"filter3"}

// @ui {"widget":"group_start", "label":"Animations"}
// @input Component.SpriteVisual f3_animation0_sq0 {"label": "Animation 0"}
// @input Component.SpriteVisual f3_animation0_sq1 {"label": "Animation 1"}
// @ui {"widget":"group_end"}

// @input Component.SpriteVisual[] statics3 {"label": "Statics"}

// @ui {"widget":"group_end"}

// @ui {"widget":"separator"}

//  @input float TriggerDisableTime = 0

'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
var triggerStartTime = getTime() - script.TriggerDisableTime;

var target_view = 0;
var animations = [];
var statics = [];

var filters_info = [
{
    name: 'filter_0',
    animations: [
    {
        type: 'single',
        sequence: script.f0_animation0_sq0
    }],
    statics: script.statics0
}, 
{
    name: 'filter_1',
    animations: [
    {
        type: 'looping',
        sequence: script.f1_animation0_sq0
    }, 
    {
        type: 'single',
        sequence: script.f1_animation1_sq0
    },
    {
        type: 'looping',
        sequence: script.f1_animation1_sq1
    }],
    statics: script.statics1
},
{
    name: 'filter_2',
    animations: [
    {
        type: 'looping',
        sequence: script.f2_animation0_sq0
    },
    {
        type: 'looping',
        sequence: script.f2_animation1_sq0
    },
    {
        type: 'looping',
        sequence: script.f2_animation2_sq0
    }],
    statics: script.statics2
},
{
    name: 'filter_3',
    animations: [
    {
        type: 'two_part',
        part_0:
        {
            type: 'single',
            sequence: script.f3_animation0_sq0
        },
        part_1:
        {
            type: 'looping',
            sequence: script.f3_animation0_sq1
        }
    }],
    statics: script.statics3
}];

function onTriggered()
{
    print("Script: " + script.Trigger + " triggered");

    setFilter('filter_' + target_view);
    target_view = target_view + 1 === filters_info.length ? 0 : target_view + 1;
}

function triggerCallback()
{ 
    if (getTime() >= triggerStartTime + script.TriggerDisableTime) {
        triggerStartTime = getTime();
        onTriggered();
    }
}

var Static = function() 
{
    function Static(json) 
    {
        _classCallCheck(this, Static);

        this.sprite = json;
        this.statics = [];
        this.load();
    }

    _createClass(Static, [{
        key: 'load',
        value: function load() {

            this.statics.push(this.sprite);
            this.sprite.getSceneObject().enabled = true;
        }
    },
    {
        key: 'cancel',
        value: function cancel() {
            this.statics.forEach(function (a) {
                a.getSceneObject().enabled = false;
            })
        }
    }]);

    return Static;
}();

var Animation = function () 
{
    function Animation(json) 
    {
        _classCallCheck(this, Animation);

        this.info = json;
        this.animations = [];
        this.load();
    }

    _createClass(Animation, [{
        key: 'load',
        value: function load() {
            switch (this.info.type) {
                case 'single':
                    this.loadSingle();
                    break;
                case 'looping':
                    this.loadLooping();
                    break;
                case 'two_part':
                    this.loadTwoPart();
                    break;
                default:
                    print('unknown animation type');
                    print(this.info.type);
                    break;
            }
        }
    }, {
        key: 'loadSingle',
        value: function loadSingle() {
            var _this = this;
            
            var sprite = this.info.sequence;
            this.animations.push(sprite);
            
            var provider = sprite.mainPass.baseTex.control;
           
            sprite.getSceneObject().enabled = true;
            provider.play(1, 0);
        }
    }, {
        key: 'loadLooping',
        value: function loadLooping() {
            var _this2 = this;
            
            var sprite = this.info.sequence;
            this.animations.push(sprite);
            
            var provider = sprite.mainPass.baseTex.control;
           
            sprite.getSceneObject().enabled = true;
            provider.play(-1, 0);
        }
    }, {
        key: 'loadTwoPart',
        value: function loadTwoPart() {
            var _this3 = this;

            var sprite0 = this.info.part_0.sequence;
            var sprite1 = this.info.part_1.sequence;  
            this.animations.push(sprite0);
            this.animations.push(sprite1);

            var provider0 = sprite0.mainPass.baseTex.control;
            var provider1 = sprite1.mainPass.baseTex.control;

            sprite0.getSceneObject().enabled = true;
            provider0.play(1, 0);
            
            provider0.setOnFinish(function () {

                sprite1.getSceneObject().enabled = true;
                provider1.play(-1,0);

                sprite0.getSceneObject().enabled = false;
            });

            var initiated_playback_sq1 = false;
            // setup second sequence (looping/single playback)
            switch (this.info.part_1.type) {
                case 'single':
                    initiated_playback_sq1 = true;
                    break;
                case 'looping':
                    initiated_playback_sq1 = true;
                    break;
                default:
                    break;
            }

            switch (this.info.part_0.type) {
                case 'single':

                    break;
                case 'looping':

                    break;
                default:
                    break;
            }

            // set first animation to start second animation on end
        }
    }, {
        key: 'cancel',
        value: function cancel() {
            this.animations.forEach(function (a) {
                a.getSceneObject().enabled = false;
                a.mainPass.baseTex.control.stop();
            });
            this.animations = [];
        }
    }]);

    return Animation;
}();

function setFilter(filter) 
{
    animations.forEach(function (a) {
        return a.cancel();
    });
    animations = [];

    statics.forEach(function (a) {
        return a.cancel();
    });
    statics = [];
    
    var target;
    filters_info.forEach(function (a) {
        if(a.name === filter)
        {
            target = a;
        }
    });

    target.animations.forEach(function (animation) {
        animations.push(new Animation(animation));
    });
    
    for (var i = 0; i < target.statics.length; ++i) 
    {
        statics.push(new Static(target.statics[i]));
    }
}

// Allow fullscreen tapping if trigger is touch based
if (script.Trigger == "TouchStartEvent"
    || script.Trigger == "TouchStartEvent"
    || script.Trigger == "TapEvent")
{
    global.touchSystem.touchBlocking = true;
    global.touchSystem.enableTouchBlockingException("TouchTypeDoubleTap", true);
    global.touchSystem.enableTouchBlockingException("TouchTypeSwipe", true);
}

var event = script.createEvent(script.Trigger);
event.bind(triggerCallback);